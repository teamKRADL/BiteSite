Dear Users,

Welcome to BiteSite. Here on GitLab you can find all of the code that we have used to create the site, found at community.dur.ac.uk.

The source code found here is all free to use on whatever level you would like!

Thanks!
TeamKRADL (Team 6)