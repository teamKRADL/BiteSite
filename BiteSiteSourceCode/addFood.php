<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Freelancer - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/homepagecss.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="homepage.html">Bite Site</a>
            </div>
			
			<div class="navbar-header page-scroll" style="margin-left:200px;">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Profile
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					  <li><a href="#">View Profile</a></li>
					  <li><a href="#">Edit Profile</a></li>
					</ul>
			    </li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Food
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					  <li><a href="viewFood.html">View Food</a></li>
					  <li><a href="addFood.html">Add Food Item</a></li>
					</ul>
			    </li>
			</ul>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="index.html">Logout</a>
                    </li>

                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
	
	<!-- Header -->
    <header>
        <div class="container">
			<div class="addFood">
			<h1>Add a Food Item</h1>
			<br>
			<form name="foodForm">
				<div class="form-group">
					<label>Item Name:</label>
					<input type="text" class="form-control" id="itemName" placeholder="Item Name" name="itemName>
				</div>
				<div class="form-group">
					<label>Description:</label>
					<input type="text" class="form-control" id="description" placeholder="Description" name="description>
				</div>
				<div class="form-group">
					<label>Picture:</label>
					<input type="file" name="img" id="img">
				</div>
				<!--<div class="checkbox">
					<label><input type="checkbox">Visible</label>
				</div>-->
				<button type="button" onclick="addFood()">Submit</button>
			</form>
			</div>
		</div>
    </header>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>

	<script>

function addFood(){

	console.log("addFoodTest");
	var str=""; 
	var strImg="";
	var id="";
	var saleID="";
	var itemName = $("#itemName").val();
	var description = $("#description").val();
	var usernameSeller="";
	var datePosted="";
	var dateSold="";
	var usernameBuyer="";
	
	
	
	//var image = $("#img").val();
	//var image = $('#img').prop('files');

	
	var image = " <?php $image = addslashes(file_get_contents($_FILES['img']['tmp_name'])); echo $image; ?> ";
	
	var d = new Date();
	var day = d.getDate() + 1;
	var month = d.getMonth() + 1;
	var year = d.getFullYear();
	
	datePosted = year + "-" + month + "-" + day;
	console.log(datePosted);

	$.post("http://community.dur.ac.uk/cs.seg06/AddOrMarkSoldItem.php",
    {
        itemName: itemName,
        itemDescription: description,
		usernameSeller: "testSeller",
		quantity: "1",
		datePosted: datePosted,
		image: image

    },
    function(data, status){
        alert("Data: " + data + "\nStatus: " + status);
    });
	
	/**$.post("http://community.dur.ac.uk/cs.seg06/PHPimages.php",
    {
        image: image
    },
    function(data, status){
        alert("Data: " + data + "\nStatus: " + status);
    });
	**/
}

$( document ).ready(function(){

})
</script>
</body>



</html>
