<?php


echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>';

// Use conn.php to connect to MySQL db
include 'conn.php';
$db = new DB();
$con = $db->connect();
$method = $_SERVER['REQUEST_METHOD'];

// Check if request is GET or POST
// GET serves the my requests page
// POST serves the individual item page where user can make a request
if ($method == "GET" and isset($_GET['user'])) {
  $user = $_GET['user'];

  // Retrieve outgoing requests - requests the user has made
  echo "<div class='container' style='background-color:#ffffff'><div id='outgoing' ><h3> OUTGOING REQUESTS</h3>";
  $sql = mysqli_query($con, "SELECT ItemsSale.itemName, ItemsSale.usernameSeller, Request.dateTime, Request.itemID FROM ItemsSale, Request WHERE Request.requester = '$user' AND Request.itemID = ItemsSale.saleID AND ItemsSale.usernameSeller != '$user' ORDER BY Request.dateTime DESC ");

  echo "<table border=1>
        <tr><th>Item name </th><th>Seller </th><th>Date and time of request</th></tr> ";
  while ($row = mysqli_fetch_assoc($sql)){

    echo "<tr><td>".$row[itemName]. "</td><td>" . $row[usernameSeller]. "</td><td>". $row[dateTime] ."</td><td> <button onclick=cancelReq(".$row[itemID].",".json_encode($user).")> Cancel </button></td></tr>";

  }

  echo "</table></div>";

  echo "<div id='incoming'><h3> INCOMING REQUESTS</h3>";
  $sql = mysqli_query($con, "SELECT ItemsSale.itemName, Request.requester, Request.dateTime, Request.itemID FROM ItemsSale, Request WHERE ItemsSale.usernameSeller = '$user' AND Request.itemID = ItemsSale.saleID AND Request.requester != '$user' ORDER BY Request.dateTime DESC ");

  echo "<table border=1>
        <tr><th>Item name </th><th>Requester </th><th>Date and time of request</th></tr> ";
  while ($row = mysqli_fetch_assoc($sql)){

    echo "<tr><td>".$row[itemName]. "</td><td>" . $row[requester]. "</td><td>". $row[dateTime] ."</td><td><button onclick=acceptReq(".$row['itemID'].",".json_encode($row[requester]).")>Accept</button></td></tr>";

  }

  echo "</table></div><br></div>";

  ?>

  <script>

  function acceptReq(itemID, requester){

    //console.log("sup");
    console.log(itemID + requester);

    $.ajax({
      type: 'POST',
      url: 'http://community.dur.ac.uk/cs.seg06/acceptReq.php',
      data: {"itemID": itemID, "requester" : requester},
      dataType: 'text',
      success: function(result){
        alert ( result)
      }
    });


  }

  function cancelReq(itemID, user){

    //console.log("sup");
    console.log(itemID + user);

    $.ajax({
      type: 'POST',
      url: 'http://community.dur.ac.uk/cs.seg06/acceptReq.php',
      data: {"itemID": itemID, "user" : user},
      dataType: 'text',
      success: function(result){
        alert ( result)
      }
    });


  }



  </script>


  <?php

} else if ($method == "POST") {

  // For inserting a request get the user and itemid
  $user = $_POST['user'];
  $itemID = $_POST['id'];

  $sql =  "INSERT INTO Request (requester, itemID, dateTime, seen)
    VALUES ('$user' , '$itemID' , NOW() , 0);";

  // Add to database
  if (mysqli_query($con, $sql)){
    $response = "Request successfully added, you should message the seller for further details.";
  } else {
    $response = "Request could not be added";
  }

  // Return response to users
  //$response = json_encode($response);
  echo $response;

}

?>
