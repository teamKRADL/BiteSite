<?php
	// $username="dcs8s06";
	// $password="ru37nner";
	// $database="Pdcs8s06_FoodWaste";
	// $con = mysqli_connect("mysql.dur.ac.uk",$username,$password,$database) or die("Error connecting to server");

	//"image" => base64_encode($rows['image'])

	include 'conn.php';
	$db = new DB();
	$con = $db->connect();

	$username = $_REQUEST['username'];
	$id = $_REQUEST['id'];



	if(isset($username)){
		//IF USERNAME IS ENTERED, RETURN IMAGES
		$sql = mysqli_query($con, "SELECT * FROM ItemsSale WHERE usernameSeller = '$username' ORDER BY datePosted DESC");
		$itemsSale = array();
		
		while($rows = mysqli_fetch_assoc($sql)) {
			$current = array("saleID" => $rows['saleID'], "itemName" => $rows['itemName'], 
							"description" => $rows['itemDescription'], "quantity"=> $rows['quantity'], "usernameSeller" => $rows['usernameSeller'],
							"datePosted" => $rows['datePosted'], "dateSold" => $rows['dateSold'], "usernameBuyer" => $rows['usernameBuyer'],
							"imageReference" => $rows['imageReference']);
			$itemsSale[] = $current;
		}

	} else if(isset($id)){
		//IF ID IS ENTERED, RETURN IMAGE JUST FOR THAT saleID
		$sql = mysqli_query($con, "SELECT * FROM ItemsSale WHERE saleID = '$id' ORDER BY datePosted DESC");
		$itemsSale = array();
		
		while($rows = mysqli_fetch_assoc($sql)) {
			$current = array("saleID" => $rows['saleID'], "itemName" => $rows['itemName'], 
							"description" => $rows['itemDescription'], "quantity"=> $rows['quantity'], "usernameSeller" => $rows['usernameSeller'],
							"datePosted" => $rows['datePosted'], "dateSold" => $rows['dateSold'], "usernameBuyer" => $rows['usernameBuyer'],
							"imageReference" => $rows['imageReference']);
			$itemsSale[] = $current;
		}

	} else {
		//IF USERNAME IS NOT ENTERED, DON'T RETURN IMAGES
		$sql = mysqli_query($con, "SELECT saleID, itemName, itemDescription, usernameSeller, datePosted, dateSold, usernameBuyer, imageReference  FROM ItemsSale ORDER BY datePosted DESC");
		$itemsSale = array();
		
		while($rows = mysqli_fetch_assoc($sql)) {
			$current = array("saleID" => $rows['saleID'], "itemName" => $rows['itemName'], 
							"description" => $rows['itemDescription'], "quantity"=> $rows['quantity'], "usernameSeller" => $rows['usernameSeller'],
							"datePosted" => $rows['datePosted'], "dateSold" => $rows['dateSold'], "usernameBuyer" => $rows['usernameBuyer'], 
							"imageReference" => $rows['imageReference']);
			$itemsSale[] = $current;
		}
	}
		

	//Form JSON response
	$itemsSaleList = array("items" => $itemsSale);
	$json_response = json_encode($itemsSaleList);
	echo $json_response; 
?> 