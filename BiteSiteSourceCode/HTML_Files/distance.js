// contains all the items currently available (by ajax request)
var items = [];

// fetches all the items and puts them into the items array
$.ajax({
  type: "GET",
  dataType: "json",
  async: false,
  url: "http://community.dur.ac.uk/cs.seg06/Address.php", 
  success: function(data) {
    var latlngs = data["latlng"];
    items = latlngs;
  }
});


function filterByRad(username, user_distance) {
  // loops through the items and gets the location corresponding to the user id
  for(item in items) {
    if(items[item][3] === username) {
      var user_lat_deg = items[item][0];
      var user_long_deg = items[item][1];
      items.splice(item, 1);
      var filteredItems = getDistances(user_lat_deg, user_long_deg, user_distance);
      return filteredItems;
    };
  };
};


function getDistances(user_lat_deg, user_long_deg, user_distance) {
  // will contain all items within the user defined radius after execution of the code
  var items_in_radius = [];

  // mean radius of the earth
  var R = 6371e3;

  // iterates through the items in the item array
  for(item in items) {
    var user_lat = user_lat_deg * Math.PI / 180;
    var curr_lat = items[item][0] * Math.PI / 180;

    var lat_diff = (items[item][0] - user_lat_deg) * Math.PI / 180;
    var long_diff = (items[item][1] - user_long_deg) * Math.PI / 180;

    var a = Math.sin(lat_diff/2) * Math.sin(lat_diff/2) +
        Math.cos(user_lat) * Math.cos(curr_lat) *
        Math.sin(long_diff/2) * Math.sin(long_diff/2);

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    var d = (R * c) / 1000;
    console.log(d);

    if (d <= user_distance) {
      items_in_radius.push(items[item][3]);
    };
  };
  return items_in_radius;
};

console.log(filterByRad("AtharvaD", 500));


