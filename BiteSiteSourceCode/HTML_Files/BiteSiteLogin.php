<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Log-in</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/signUpcss.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


  <!-- <title>BiteSite Login</title>
  <meta charset="utf-8">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
  <style>
    h1 {
      color: #000000;
      text-align: center;
    }
    h3 {
      text-align: center;
    }
    /*.navbar.navbar-default li a{
      color: #992600;
    }
    .navbar.navbar-default a{
      color: #992600;
    }*/
    /*.row {
      margin-left: auto;
      margin-right: auto;
      text-align: center;
      width: 100%;
    }
    th {
      margin-left: auto;
      margin-right: auto;
      text-align: center;
      width: 100%;
    }*/

    .wrapper {
    width: 30%;
    position: absolute;

    left: 0;
    right: 0;

    margin-left: auto;
    margin-right: auto;
}
  </style>
</head>

    <body>
    	<!-- Navigation -->
	    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
	        <div class="container">
	            <!-- Brand and toggle get grouped for better mobile display -->
	            <div class="navbar-header page-scroll">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
	                </button>
	                <a class="navbar-brand" href="#page-top">Bite Site</a>
	            </div>

	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	                <ul class="nav navbar-nav navbar-right">
	                    <li class="hidden">
	                        <a href="#page-top"></a>
	                    </li>
	                    <li class="page-scroll">
	                        <a href="#about">About</a>
	                    </li>

	                </ul>
	            </div>
	            <!-- /.navbar-collapse -->
	        </div>
	        <!-- /.container-fluid -->
	    </nav>


        <div class="wrapper">
            <div class="page-header">
				<h1 >    <br><br>Bite Site Log-In </h1><br>
            </div>
            <!-- row -->
            <form class="form-signin" onsubmit="return false" method="FORM">
				<label for="inputEmail" class="sr-only">Email address</label>
				<input id="inputUsername" class="form-control" placeholder="Username" required autofocus>
				<label for="inputPassword" class="sr-only">Password</label>
				<input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
				<div class="checkbox">
					<label><input type="checkbox" value="remember-me" id="checkbox"> Remember me</label>
				</div>
				<button id="submitbtn" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
			</form>
			<p id="feedback"></p>
		</div>

		<script>
			$(document).ready(function() {
				$(window).keydown(function(event){
					if(event.keyCode == 13) {
						event.preventDefault();
						$('#submitbtn').trigger('click');
						return false;
					}
				});

				if(localStorage.username!=null && localStorage.password!=null){
					document.getElementById("inputUsername").value = localStorage.username;
					document.getElementById("inputPassword").value = localStorage.password;
				}
			});

			$("#submitbtn").on( "click", function() {
				var username = jQuery('input[id="inputUsername"]').val();
				var password = jQuery('input[id="inputPassword"]').val();
        var matchFound = false;


        //GET USERNAMES AND PASSWORDS, CHECK FOR MATCHES
        $.get('http://community.dur.ac.uk/cs.seg06/Users.php', function(data,status){
            var returnedJson = $.parseJSON(data);
            var userArray = returnedJson['users'];

            for(i = 0; i < userArray.length; i++){
              var expectedUsername = userArray[i]['username'];
              var expectedPassword = userArray[i]['password'];
              if(username == expectedUsername && password == expectedPassword){
                  matchFound = true;
              }
            }

          if(matchFound){
            console.log("it was found");
            localStorage.username = username;
						<?php
							session_start();
							$_SESSION["username"] = ?> username <? ;
						 ?>
            if(document.getElementById('checkbox').checked) {
              console.log('CHECKED');
              localStorage.password = password;
              sessionStorage.validated = 'YES';
            }
            window.location.replace("http://community.dur.ac.uk/cs.seg06/Group%20Project%20html/viewFood.html");
          } else {
            document.getElementById('feedback').innerHTML = 'Incorrect Username and/or Password. Try Again';
          }
        }); //GET


      }); //OnClick
		</script>
    </body>

</html>
