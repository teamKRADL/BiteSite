// contains all the items currently available (by ajax request)
var items = [];

// fetches all the items and puts them into the items array
$.ajax({
  type: "GET",
  dataType: "json",
  async: false,
  url: "http://community.dur.ac.uk/cs.seg06/Address.php", 
  success: function(data) {
    var latlngs = data["latlng"];
    items = latlngs;
  }
});


function isInRad(current_user, dist_username, user_distance) {
  // loops through the items and gets the location corresponding to the user id
  for(item in items) {
    if(items[item][3] === current_user) {
      var user_lat_deg = items[item][0];
      var user_long_deg = items[item][1];
      var filteredItems = getDistance(user_lat_deg, user_long_deg, user_distance, dist_username);
      return filteredItems;
    };
  };
};


function getDistance(user_lat_deg, user_long_deg, user_distance, dist_username) {
  var dist_user_lat = null;
  var dist_user_long = null;

  for(item in items) {
    if(items[item][3] === dist_username) {
      dist_user_lat = items[item][0];
      dist_user_long = items[item][1];
      break;
    }
  }

  if(dist_user_lat === null) {
    console.log("username undefined");
    return false;
  }

  // mean radius of the earth
  var R = 6371e3;
  
  var user_lat = user_lat_deg * Math.PI / 180;
  var curr_lat = dist_user_lat * Math.PI / 180;

  var lat_diff = (dist_user_lat - user_lat_deg) * Math.PI / 180;
  var long_diff = (dist_user_long - user_long_deg) * Math.PI / 180;

  var a = Math.sin(lat_diff/2) * Math.sin(lat_diff/2) +
      Math.cos(user_lat) * Math.cos(curr_lat) *
      Math.sin(long_diff/2) * Math.sin(long_diff/2);

  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  var d = (R * c) / 1000;
  console.log(d);

  if (d <= user_distance) {
    return true;
  } else {
    return false;
  }
};

console.log(isInRad("JLeyland96", "AtharvaD", 0.1));