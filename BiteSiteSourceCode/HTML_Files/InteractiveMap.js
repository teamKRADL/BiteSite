var markers = [];

$.ajax({
  type: "GET",
  dataType: "json",
  async: false,
  url: "http://community.dur.ac.uk/cs.seg06/Address.php", 
  success: function(data) {
    var latlngs = data["latlng"];
    markers = latlngs;
  }
});

function initMap() {
    var durham = {lat: 54.7753,lng: -1.5345};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 9,
      center: durham,
    });

    var infowindow = new google.maps.InfoWindow();

    for (marker in markers) {
      var thisuser = markers[marker][3];
      var location = "http://community.dur.ac.uk/cs.seg06/Group%20Project%20html/viewProfile.html?username=" + thisuser;

    	var temp = new google.maps.Marker({
    		position: {lat: markers[marker][0], lng: markers[marker][1]},
    		map: map,
        content: "User:" + " " + markers[marker][3] + '<br/>' + "Address:" + " " + markers[marker][4] + '<br/>' + "Items:" + " " + markers[marker][2] + '<br/>' + '<br/>' + '<hr>' + '<a href="' + location + '">View User Profile</a>'
    	});

      google.maps.event.addListener(temp, 'click', function() {
        infowindow.setContent(this.content);
        infowindow.open(map, this);
      });

    };
}

function getUserPage(username) {
  window.open("http://community.dur.ac.uk/cs.seg06/Group%20Project%20html/viewProfile.html?username=" + username);
}
